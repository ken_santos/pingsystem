// Copyright Epic Games, Inc. All Rights Reserved.

#include "PingSystem.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, PingSystem, "PingSystem" );
 