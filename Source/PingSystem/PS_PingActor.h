// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PS_Structs.h"
#include "PS_PingActor.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDefaultPingActorDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FUpdateWidgetStateDelegate, EPS_AnimationIconState, NewState);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FActivateWidgetDelegate, FVector, Location, FPS_IconSettings, Settings);

UCLASS(Blueprintable)
class PINGSYSTEM_API APS_PingActor : public AActor
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	APS_PingActor();
	// Components
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Components")
	class UWidgetComponent* PingWidgetComponent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Components")
	class UPS_PingComponent* OwningPingComponent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Components")
	class UAudioComponent* HoldSound;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General")
	bool bInUse;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General")
	bool bIsOnScreen;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General")
	int OwnerID;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General")
	FVector Location;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General")
	AActor* AttachedActor;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General")
	FPS_IconSettings Settings;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General")
	class APlayerController* PlayerController;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General")
	bool bUseScreenLocationCheck;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General")
	FTimerHandle PhaseTimer;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General")
	float ScreenRotation;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General")
	FVector2D ScreenOffset;



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	UFUNCTION(BlueprintCallable, Category = "Core")
	void UpdateScreenLocation();

	UFUNCTION(BlueprintCallable, BlueprintPure, Category = "Core")
	FVector GetLocation();

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Core")
	void Initialize(class UPS_PingComponent* PingComponent);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Core")
	void Activate(FVector InLocation, class AActor* InActor, FPS_IconSettings InSettings, int InOwnerID);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Core")
	void Deactivate(bool Force = false);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Screen Events")
	void OnEnterScreen();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Screen Events")
	void OnExitScreen();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Screen Events")
	void ResetScreenEvents();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "State Transitions")
	void OnHold();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "State Transitions")
	void OnExit();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "State Transitions")
	void OnComplete();

public:
	// EventDispatchers
	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Event Dispatchers")
	FUpdateWidgetStateDelegate UpdateWidgetState;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Event Dispatchers")
	FActivateWidgetDelegate ActivateWidget;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Event Dispatchers")
	FDefaultPingActorDelegate DeactivateWidget;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Event Dispatchers")
	FDefaultPingActorDelegate EnteredScreen;

	UPROPERTY(BlueprintAssignable, BlueprintCallable, Category = "Event Dispatchers")
	FDefaultPingActorDelegate ExitedScreen;
};