// Fill out your copyright notice in the Description page of Project Settings.


#include "PS_Structs.h"

FPS_TableIconInfo::FPS_TableIconInfo()
{
}

FPS_TableIconInfo::FPS_TableIconInfo(bool pTableFound, UDataTable* pTable, FName pTableID, bool pIconFound, FName pIconID, FPS_IconSettings pIconSettings)
{
	bTableFound = pTableFound;
	Table = pTable;
	TableID = pTableID;
	bIconFound = pIconFound;
	IconID = pIconID;
	IconSetting = pIconSettings;
}
