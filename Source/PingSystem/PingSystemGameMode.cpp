// Copyright Epic Games, Inc. All Rights Reserved.

#include "PingSystemGameMode.h"
#include "PingSystemHUD.h"
#include "PingSystemCharacter.h"
#include "UObject/ConstructorHelpers.h"

APingSystemGameMode::APingSystemGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = APingSystemHUD::StaticClass();
}
