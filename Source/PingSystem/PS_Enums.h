// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */

UENUM(BlueprintType)
enum class EPS_AnimationIconState : uint8
{
	ENTER		UMETA(DisplayName = "Enter"),
	HOLD		UMETA(DisplayName = "Hold"),
	EXIT	    UMETA(DisplayName = "Exit")
};

UENUM(BlueprintType)
enum class EPS_AnimationIconTransition : uint8
{
	NONE		UMETA(DisplayName = "None"),
	SLIDE       UMETA(DisplayName = "Slide"),
	BOUNCE      UMETA(DisplayName = "Bounce"),
	SCALE		UMETA(DisplayName = "Scale")
};

UENUM(BlueprintType)
enum class EPS_AnimationIconLoop : uint8
{
	NONE			UMETA(DisplayName = "None"),
	PULSE_SLOW      UMETA(DisplayName = "PulseSlow"),
	PULSE_FAST      UMETA(DisplayName = "PulseFast"),
	SPIN			UMETA(DisplayName = "Spin"),
	BOUNCE			UMETA(DisplayName = "Bounce"),
};

UENUM(BlueprintType)
enum class EPS_TraceMethod : uint8
{
	CAMERA		UMETA(DisplayName = "Camera"),
	MOUSE		UMETA(DisplayName = "Mouse"),
	CUSTOM      UMETA(DisplayName = "Custom"),
};

UENUM(BlueprintType)
enum class EPS_MenuSelectionType : uint8
{
	MOUSE		UMETA(DisplayName = "Mouse"),
	GAMEPAD		UMETA(DisplayName = "Gamepad")
};