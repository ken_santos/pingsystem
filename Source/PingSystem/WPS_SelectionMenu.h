// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "PS_Structs.h"
#include "WPS_SelectionMenu.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FSelectedDelegate, FPS_IconSettings, Selected, FName, IconID);

/**
 * 
 */
UCLASS(Blueprintable)
class PINGSYSTEM_API UWPS_SelectionMenu : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Components")
	class UPS_PingComponent* PingComponent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General")
	int Divisions;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General")
	float AngleSize;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General")
	float DistanceFromCenter = 200.0f;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General")
	FVector ReferenceVector;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General")
	class UMaterialInstanceDynamic*	 RadialMenuMaterial;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General")
	FVector2D MenuCenter;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General")
	int SelectedIndex = -1;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General")
	int StartIndex = 1;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General")
	float SelectionToleranceFromCenter = 125.0f;

public:
	UFUNCTION(BlueprintCallable, Category = "General")
	void ConfirmSelection();

	UFUNCTION(BlueprintCallable, Category = "General")
	void CancelSelection();

public:
	UPROPERTY(BlueprintCallable, BlueprintAssignable, Category = "Event Dispatchers")
	FSelectedDelegate Selected;
};
