// Fill out your copyright notice in the Description page of Project Settings.


#include "PSFL_UtilitiesLibrary.h"
#include "GameFramework/PlayerController.h"
#include "Blueprint/WidgetLayoutLibrary.h"

float UPSFL_UtilitiesLibrary::GetSinFrequency(float Seed, float Speed, float WorldSeconds)
{
	return ((FMath::Sin(FMath::DegreesToRadians(Speed * (Seed + WorldSeconds))) + 1.0f) / 2.0f);
}

bool UPSFL_UtilitiesLibrary::IsLocationOffScreen(APlayerController* Controller, FVector Location, float EdgePercent)
{
	FVector2D ViewportSize = UWidgetLayoutLibrary::GetViewportSize(Controller) * UWidgetLayoutLibrary::GetViewportScale(Controller);

	
	FVector2D ScreenPosition;
	bool OnScreen = Controller->ProjectWorldLocationToScreen(Location, ScreenPosition);
	ScreenPosition *= UWidgetLayoutLibrary::GetViewportScale(Controller);

	FVector2D ViewportCenter = ViewportSize * 0.5f;
	FVector2D ScreenBounds = ViewportCenter * EdgePercent;
	
	if (OnScreen) {
		return (ScreenPosition.X < (ViewportCenter.X - ScreenBounds.X)) || 
			(ScreenPosition.X > (ViewportCenter.X + ScreenBounds.X)) ||
			(ScreenPosition.Y < (ViewportCenter.Y - ScreenBounds.Y)) ||
			(ScreenPosition.Y > (ViewportCenter.Y + ScreenBounds.Y));
	}

	return true;
}

bool UPSFL_UtilitiesLibrary::CalculateLocationToEdgeScreenPosition(APlayerController* Controller, FVector Location, float EdgePercent, FVector2D& ScreenLocation, float& Rotation)
{
	APlayerController* PlayerController = Controller;
	FVector TargetLocation = Location;
	float Edge = EdgePercent;

	FVector2D ViewportSize = UWidgetLayoutLibrary::GetViewportSize(Controller);
	FVector2D ViewportCenter = ViewportSize * 0.5f;
	FVector2D ScreenPosition;
	Controller->ProjectWorldLocationToScreen(TargetLocation, ScreenPosition);

	float Invert = 1.0f;
	float FinalRotation = 0.0f;
	UE_LOG(LogTemp, Warning, TEXT("UPSFL_UtilitiesLibrary::CalculateLocationToEdgeScreenPosition::ScreenPosition - {%f, %f}"), ScreenPosition.X, ScreenPosition.Y);
	if ((ScreenPosition.X <= 0.0001f  && ScreenPosition.X >= -0.0001f)) {
		// If position X == 0, it probably can't be projected (it's behind us), so we cheat and use inverted forward offset and invert again at end of calculation
		Controller->ProjectWorldLocationToScreen((((TargetLocation - Controller->PlayerCameraManager->GetCameraLocation()) * -1.0f) + 
			Controller->PlayerCameraManager->GetCameraLocation()), ScreenPosition);

		Invert = -1.0f;
		FinalRotation = 180.0f;
	}


	// Assign boundsand convert screen position relative to screen center
	FVector2D ScreenBounds = ViewportCenter * Edge;
	ScreenPosition -= ViewportCenter;
	
	// Core math
	float AngleRadians = FMath::Atan2(ScreenPosition.Y, ScreenPosition.X) - FMath::DegreesToRadians(90.0f);
	FinalRotation += FMath::RadiansToDegrees(AngleRadians) + 180.0f;

	float Cosine = FMath::Cos(AngleRadians);
	float Sine = FMath::Sin(AngleRadians) * -1.0f;

	ScreenPosition.X = ViewportCenter.X + (Sine * 150.0f);
	ScreenPosition.Y = ViewportCenter.Y + (Cosine * 150.0f);

	float M = Cosine / Sine;

	// Build final position based on math properties
	if (Cosine > 0.0f) {
		ScreenPosition.X = ScreenBounds.Y / M;
		ScreenPosition.Y = ScreenBounds.Y;
	}
	else {
		ScreenPosition.X = (ScreenBounds.Y / M) * -1.0f;
		ScreenPosition.Y = ScreenBounds.Y * -1.0f;
	}

	if (ScreenPosition.X > ScreenBounds.X) {
		ScreenPosition.X = ScreenBounds.X;
		ScreenPosition.Y = ScreenBounds.X * M;
	}
	else if (ScreenPosition.X < ScreenBounds.X * -1.0f) {
		ScreenPosition.X = ScreenBounds.X * -1.0f;
		ScreenPosition.Y = (ScreenBounds.X * M) * -1.0f;
	}
	
	FVector2D NewScreenLocation = (ScreenPosition * Invert) + ViewportCenter;
	ScreenLocation = FVector2D(NewScreenLocation.X, FMath::Max(NewScreenLocation.Y, 96.0f));
	Rotation = FinalRotation;

	return true;
}
