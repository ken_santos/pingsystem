// Fill out your copyright notice in the Description page of Project Settings.


#include "PS_PingActor.h"
#include "Components/WidgetComponent.h"
#include "Components/AudioComponent.h"
#include "Sound/SoundBase.h"
#include "GameFramework/PlayerController.h"
#include "PS_Structs.h"
#include "PSFL_UtilitiesLibrary.h"
#include "PS_PingComponent.h"

// Sets default values
APS_PingActor::APS_PingActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	PingWidgetComponent = CreateDefaultSubobject<UWidgetComponent>("PingWidgetComponent");
	if (PingWidgetComponent) {

		PingWidgetComponent->SetupAttachment(RootComponent);

		// WidgetClass
		PingWidgetComponent->SetWidgetSpace(EWidgetSpace::Screen);
		PingWidgetComponent->SetDrawSize(FVector2D(128, 128));
		PingWidgetComponent->SetDrawAtDesiredSize(true);

		PingWidgetComponent->SetGenerateOverlapEvents(false);
		PingWidgetComponent->CanCharacterStepUpOn = ECanBeCharacterBase::ECB_No;
		PingWidgetComponent->SetCollisionProfileName(FName("NoCollision"));

		PingWidgetComponent->OverrideMaterials.Add(NULL);
		PingWidgetComponent->bUseAttachParentBound = true;
	}

	bAllowReceiveTickEventOnDedicatedServer = false;
	SetTickGroup(ETickingGroup::TG_PostUpdateWork);

	bNetLoadOnClient = false;
	bReplicates = true;

	SetCanBeDamaged(false);

	bInUse = true;
	bUseScreenLocationCheck = true;
}

// Called when the game starts or when spawned
void APS_PingActor::BeginPlay()
{
	Super::BeginPlay();
}

void APS_PingActor::UpdateScreenLocation()
{
	if (bUseScreenLocationCheck) {
		bool OffScreen = UPSFL_UtilitiesLibrary::IsLocationOffScreen(PlayerController, GetLocation(), Settings.Advanced.PeripheralEdgePercent);

		if (OffScreen) {
			OnExitScreen();
			UPSFL_UtilitiesLibrary::CalculateLocationToEdgeScreenPosition(PlayerController, GetLocation(), Settings.Advanced.PeripheralEdgePercent, ScreenOffset, ScreenRotation);
		}
		else {
			OnEnterScreen();
		}
	}
}

FVector APS_PingActor::GetLocation()
{
	if (IsValid(AttachedActor)) 
		return AttachedActor->GetActorTransform().TransformPosition(Location);
	
	return Location;
}

// Called every frame
void APS_PingActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}