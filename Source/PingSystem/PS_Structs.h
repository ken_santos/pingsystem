// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "PS_Enums.h"
#include "PS_Structs.generated.h"
/**
 * 
 */
USTRUCT(BlueprintType)
struct FPS_CustomPingSettings : public FTableRowBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FName Icon;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) TArray<FString> Conditions;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) bool Attach;
};

USTRUCT(BlueprintType)
struct FPS_ActivePing : public FTableRowBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FVector Location;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FName Type;
};

USTRUCT(BlueprintType)
struct FPS_ControllerTimeoutCount : public FTableRowBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere) int Count;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) float Timeout;
};

USTRUCT(BlueprintType)
struct FPS_CurrentExecutingPing : public FTableRowBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FName ExecutingID;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) TArray<FString> ExecutingConditions;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FVector ExecutingLocation;
};

USTRUCT(BlueprintType)
struct FPS_IconAdvancedSettings : public FTableRowBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere) bool ShowInSelectionMenu = true;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) bool ShowDistance = false;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) bool ShowPeripheral = true;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) float PeripheralEdgePercent = 0.85f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) bool AllowLocalTintOverride = false;
};

USTRUCT(BlueprintType)
struct FPS_IconAnimationSettings : public FTableRowBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere) EPS_AnimationIconTransition Enter = EPS_AnimationIconTransition::SCALE;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) float EnterDuration = 0.4f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) EPS_AnimationIconLoop Loop = EPS_AnimationIconLoop::NONE;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) float LoopDuration = 5.0f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) EPS_AnimationIconTransition Exit = EPS_AnimationIconTransition::SCALE;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) float ExitDuration = 0.4f;
};

USTRUCT(BlueprintType)
struct FPS_IconAppearanceSettings : public FTableRowBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere) float Scale = 1.0f;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FVector2D Anchor = FVector2D(0.5f);
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FLinearColor Tint = FLinearColor(0.0f, 0.0f, 0.0f, 0.0f);
};

USTRUCT(BlueprintType)
struct FPS_IconSoundSettings : public FTableRowBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere) class USoundBase* Enter;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) class USoundBase* Hold;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) class USoundBase* Exit;
};

USTRUCT(BlueprintType)
struct FPS_IconSettings : public FTableRowBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere) UTexture2D* Icon;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FText DisplayName;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FPS_IconAppearanceSettings Appearance;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FPS_IconAnimationSettings Animation;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FPS_IconSoundSettings Sound;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FPS_IconAdvancedSettings Advanced;
};

USTRUCT(BlueprintType)
struct FPS_IconTable : public FTableRowBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere) class UDataTable* Settings;
};

USTRUCT(BlueprintType)
struct FPS_TraceSettings : public FTableRowBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere) EPS_TraceMethod Method;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) float Length;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) float Radius;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) TEnumAsByte<ETraceTypeQuery> Channel;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) bool bComplex;
};

USTRUCT(BlueprintType)
struct FPS_TableIconInfo : public FTableRowBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere) bool bTableFound;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) class UDataTable* Table;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FName TableID;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) bool bIconFound;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FName IconID;
	UPROPERTY(BlueprintReadWrite, EditAnywhere) FPS_IconSettings IconSetting;

public:
	FPS_TableIconInfo();
	FPS_TableIconInfo(bool pTableFound, class UDataTable* pTable, FName pTableID, bool pIconFound, FName pIconID, FPS_IconSettings pIconSettings);
};