// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "PSFL_UtilitiesLibrary.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class PINGSYSTEM_API UPSFL_UtilitiesLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable, Category = "Utilities")
	static float GetSinFrequency(float Seed, float Speed, float WorldSeconds);

	UFUNCTION(BlueprintCallable, Category = "Utilities")
	static bool IsLocationOffScreen(class APlayerController* Controller, FVector Location, float EdgePercent);

	UFUNCTION(BlueprintCallable, Category = "Utilities")
	static bool CalculateLocationToEdgeScreenPosition(class APlayerController* Controller, FVector Location, float EdgePercent, FVector2D& ScreenLocation, float& Rotation);
};
