// Fill out your copyright notice in the Description page of Project Settings.


#include "PS_PingComponent.h"

#include "GameFramework/Pawn.h"
#include "Net/UnrealNetwork.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/DataTableFunctionLibrary.h"
#include "Blueprint/WidgetBlueprintLibrary.h"
#include "PS_PingActor.h"
#include "PS_Pingable.h"
#include "WPS_SelectionMenu.h"

#define LOCTEXT_NAMESPACE "PingSystemNamespace"

// Sets default values for this component's properties
UPS_PingComponent::UPS_PingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	SetIsReplicated(true);

	// General Settings
	SelectionMode = EPS_MenuSelectionType::MOUSE;
	TintColorList.Add(FLinearColor(1.0f, 0.647059f, 0.0f, 1.0f));
	TintColorList.Add(FLinearColor(0.0f, 0.47385f, 1.0f, 1.0f));
	TintColorList.Add(FLinearColor(0.74f, 0.041871f, 0.0f, 1.0f));
	TintColorList.Add(FLinearColor(0.192819f, 0.495f, 0.089783f, 1.0f));
	TintColorIndex = -1;
	PingPoolSize = 15;
	bAutoExpandPool = false;

	// Visibility Settings
	MinVisibleDistance = 0.0f;
	MaxVisibleDistance = 0.0f;

	// Spam Control
	TimeoutBetweenPingRequests = 0.25f;
	MaxActivePingRequests = 10;
	TimeoutOnMaxRequestsReached = 10.0f;
	MaxVisiblePingsOwner = -1;
	MaxVisiblePingsNonOwnedTotal = -1;
	MaxVisiblePingsNonOwnedPerClient = -1;

	// Trace Settings
	TraceMethod = EPS_TraceMethod::CAMERA;
	TraceLength = 200000.0f;
	TraceRadius = 15.0f;
	// TraceChannel
	bTraceComplex = false;

	// Advanced Settings
	bExecuteLocallyOnly = false;

	// Menu Settings
	BaseColor = FLinearColor(0.0f, 0.0f, 0.0f, 0.25f);	
	HoverColor = FLinearColor(0.0f, 0.0f, 0.0f, 0.75f);
	HighlightColor = FLinearColor(1.0f, 0.5f, 0.0f, 1.0f);
	HighlightThickness = 0.025f;
	BorderColor = FLinearColor(0.0f, 0.0f, 0.0f, 0.35f);
	BorderThickness = 0.0035;
	bEnableMenu = true;

	// DO NOT EDIT
	TintColor = FLinearColor(1.0f, 1.0f, 1.0f, 0.0f);
	Timeout = 0.0f;
	OverrideIcon = FName("None");
	bTraceRequired = true;
	ExecutionLocation = FVector(0.0f);
	ExecutingIcon = FName("None");
	ExecutingTable = FName("None");
	bDebug = false;
	ID = 0;
}


// Called when the game starts
void UPS_PingComponent::BeginPlay()
{
	Super::BeginPlay();

	Setup();
}

void UPS_PingComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(UPS_PingComponent, ID);
	DOREPLIFETIME(UPS_PingComponent, TintColor);
	DOREPLIFETIME(UPS_PingComponent, TintColorIndex);
}


// Called every frame
void UPS_PingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UPS_PingComponent::Setup()
{
	OwningPawn = Cast<APawn>(GetOwner());

	if (OwningPawn) {
		if (GetWorld()->IsServer()) {
			UpdateAllPingComponentReferences();

			ID = GetUniqueOwnerID();
		}

		if (OwningPawn->IsLocallyControlled()) {
			OwningPawn->OnDestroyed.AddDynamic(this, &UPS_PingComponent::OnOwnerDestroyed);

			CreateInitialPingPool();
		}
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("UPS_PingComponent::Setup::UPS_PingComponent must be attached to a pawn"));
	}
}

void UPS_PingComponent::UpdateAllPingComponentReferences()
{
	ExistingPingComponents.Empty();

	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(this, APawn::StaticClass(), FoundActors);

	for (AActor* Actor : FoundActors) {
		UPS_PingComponent* PingComponent = Cast<UPS_PingComponent>(Actor->GetComponentByClass(UPS_PingComponent::StaticClass()));

		if (IsValid(PingComponent)) {
			ExistingPingComponents.AddUnique(PingComponent);
		}
	}

	for (UPS_PingComponent* PingComponent : ExistingPingComponents) {
		PingComponent->ExistingPingComponents = ExistingPingComponents;
	}
}

APS_PingActor* UPS_PingComponent::CreatePingActorForPool()
{
	FVector Location(0.0f, 0.0f, 200.0f);
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	APS_PingActor* SpawnedPingActor = GetWorld()->SpawnActor<APS_PingActor>(PingActorToUse, Location, Rotation, SpawnInfo);
	SpawnedPingActor->Initialize(this);

	ExistingPingActors.Add(SpawnedPingActor);

	return SpawnedPingActor;
}

void UPS_PingComponent::CreateInitialPingPool()
{
	ClearPingPool();

	for (int i = 1; i <= PingPoolSize; i++) {
		CreatePingActorForPool();
	}
}

void UPS_PingComponent::ClearPingPool()
{
	for (APS_PingActor* PingActor : ExistingPingActors) {
		if (IsValid(PingActor)) {
			PingActor->Destroy();
		}
	}

	ExistingPingActors.Empty();
}

APS_PingActor* UPS_PingComponent::GetUnusedPingActorFromPool()
{
	for (APS_PingActor* Ping : ExistingPingActors) 
		if (!Ping->bInUse) 
			return Ping;
		
	if (bAutoExpandPool) 
		return CreatePingActorForPool();

	return NULL;
}

UWPS_SelectionMenu* UPS_PingComponent::ToggleSelectionMenu(bool Visible)
{
	APlayerController* Controller = UGameplayStatics::GetPlayerController(this, 0);
	if (Visible) {
		if (!IsValid(SelectionMenu)) {
			SelectionMenu = CreateWidget<UWPS_SelectionMenu>(Controller, SelectionMenuToUse);
			SelectionMenu->PingComponent = this;
		}

		SelectionMenu->AddToViewport();
		MenuActivated.Broadcast();
		UGameplayStatics::GetPlayerController(this, 0);
		UWidgetBlueprintLibrary::SetInputMode_GameAndUI(Controller, SelectionMenu);
		Controller->bShowMouseCursor = SelectionMode == EPS_MenuSelectionType::MOUSE;

		return SelectionMenu;
	}

	if (IsValid(SelectionMenu)) {
		SelectionMenu->RemoveFromParent();
		SelectionMenu = NULL;
		MenuDeactivated.Broadcast();
		UWidgetBlueprintLibrary::SetInputMode_GameOnly(Controller);
		Controller->bShowMouseCursor = false;
		return SelectionMenu;
	}
	
	return nullptr;
}

void UPS_PingComponent::ConfirmSelectionInMenu()
{
	if (IsValid(SelectionMenu)) {
		SelectionMenu->ConfirmSelection();
	}
}

void UPS_PingComponent::CancelSelectionInMenu()
{
	if (IsValid(SelectionMenu)) {
		SelectionMenu->CancelSelection();
	}
}

void UPS_PingComponent::ExecutePingOnClient(FVector Location, FString Config, int OwnerID, class AActor* AttachedActor)
{
	FName Path;
	int Index = Config.Find(":", ESearchCase::IgnoreCase, ESearchDir::FromEnd, -1);

	if (Index < 0) {
		Path = FName(*Config);
		TintColor = FLinearColor(0.0f, 0.0f, 0.0f, 0.0f);
	}
	else {
		FString PathSubstring = Config.Mid(0, Index);
		Path = FName(*PathSubstring);

		if (TintColorList.Num() == 0) {
			TintColor = FLinearColor(0.0f, 0.0f, 0.0f, 0.0f);
		}
		else {
			FString TintValueFromConfig = Config.Mid(Index + 1, Config.Len());
			int TintColorListIndex = ClampIntegerToRange(FCString::Atoi(*TintValueFromConfig), 0, TintColorList.Num() - 1, true);
			TintColor = TintColorList[TintColorListIndex];
		}
	}

	// Get Icon and Table from path
	FPS_TableIconInfo TableIconInfo = GetIconAndTableFromPath(Path);

	if (TableIconInfo.bIconFound) {
		APS_PingActor* PingActor = GetUnusedPingActorFromPool();
		// UE_LOG(LogTemp, Warning, TEXT("UPS_PingComponent::ExecutePingOnClient::CHECKING FOR ACTOR"));
		if (IsValid(PingActor)) {
			//UE_LOG(LogTemp, Warning, TEXT("UPS_PingComponent::ExecutePingOnClient::ACTIVATING PING"));
			PingActor->Activate(Location, AttachedActor, TableIconInfo.IconSetting, OwnerID);

			// Handle Active Culling
			HandleActiveCulling(PingActor);
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("UPS_PingComponent::ExecutePingOnClient::ICON_NOT_FOUND"));
	}
}

void UPS_PingComponent::HandlePingOnServer(FVector Location, FString Config, AActor* AttachedActor)
{
	PingReceivedOnServer.Broadcast();

	if (CheckRateLimiting()) {
		FName Path;
		TArray<FString> DomainConditions = GetDataFromConfig(Config, Path);

		FPS_TableIconInfo TableIconInfo = GetIconAndTableFromPath(Path); 


		if (TableIconInfo.bIconFound) {
			FPS_IconSettings CurrentIconSettings = TableIconInfo.IconSetting;
			TArray<UPS_PingComponent*> PingComponents = ExistingPingComponents;

			bool PingExecuted = false;
			for (int i = 0; i < PingComponents.Num(); i++) {
				if (IsValid(PingComponents[i])) {
					UPS_PingComponent* CurrentPingComponent = PingComponents[i];

					if (IsValidExecutionDomain(CurrentPingComponent->ValidDomains, DomainConditions)) {
						if (IsValidVisibleDistance(CurrentPingComponent->OwningPawn->GetActorLocation(), Location, CurrentPingComponent->MinVisibleDistance, CurrentPingComponent->MaxVisibleDistance)) {

							FText IconConfig = FText::Format(LOCTEXT("PING_ICON_CONFIG", ":{index}"), FText::AsNumber(TintColorIndex));

							FText Config = 
								FText::Format(LOCTEXT("PING_CONFIG", "{icon}{id}"), 
									FText::FromName(Path), 
									TintColorIndex >= 0 ? IconConfig : FText::FromString(""));

							CurrentPingComponent->ExecuteOwningClient.Broadcast(Location, Config.ToString(), ID, AttachedActor);

							PingExecuted = true;
						}
					}
				}
				else {
					ExistingPingComponents.RemoveAt(i);
				}
			}
			if (PingExecuted) {
				PingTimeouts.Add(CurrentIconSettings.Animation.EnterDuration + CurrentIconSettings.Animation.LoopDuration + CurrentIconSettings.Animation.ExitDuration + GetWorld()->GetTimeSeconds());
			}

		}
		else {
			UE_LOG(LogTemp, Warning, TEXT("UPS_PingComponent::HandlePingOnServer::ICON_NOT_FOUND"));
		}
	}
}

void UPS_PingComponent::HandlePingOnClient(FName Icon, TArray<FString> Conditions)
{
	FName Path = OverrideIcon != FName("None") ? OverrideIcon : Icon;

	ExecutingConditions = OverrideConditions.Num() > 0 ? OverrideConditions : Conditions;

	// An interface call determines if actors below the trace location contain any icon or menu overrides
	// This is used to create the dynamic menus in the demo level and allow different icon tables to be used for different objects 
	// Actor-level icon settings override any called using CreatePing or CreatePingAtLocation
	ExecutionAttachedActor = NULL;

	FHitResult TraceResult = GetHitUsingTraceSettings();

	if (TraceResult.GetActor()) {
		if (TraceResult.GetActor()->GetClass()->ImplementsInterface(UPS_Pingable::StaticClass())) {
			FPS_CustomPingSettings PingSettings = IPS_Pingable::Execute_GetCustomPingSettings(TraceResult.GetActor(), OwningPawn);

			if (PingSettings.Attach) {
				ExecutionAttachedActor = TraceResult.GetActor();
			}

			Path = PingSettings.Icon != FName("None") ? PingSettings.Icon : Path;

			ExecutingConditions = PingSettings.Conditions.Num() > 0 ? PingSettings.Conditions : ExecutingConditions;
		}

		if (bTraceRequired) {
			if (IsValid(ExecutionAttachedActor)) {
				ExecutionLocation = ExecutionAttachedActor->GetActorTransform().InverseTransformPosition(TraceResult.ImpactPoint);
			}
			else {
				ExecutionLocation = !TraceResult.bBlockingHit ? TraceResult.TraceEnd : TraceResult.Location;
			}
		}

		FPS_TableIconInfo TableIconInfo = GetIconAndTableFromPath(Path);
		ExecutingTable = TableIconInfo.TableID;
		ExecutingIcon = TableIconInfo.IconID;

		if (TableIconInfo.bIconFound) {
			RequestPingOnServer();
		}
		else if (bEnableMenu) {
			TArray<FPS_IconSettings> TempSettings;
			ExecutingMenuIconIDs = GetMenuIconsFromTable(ExecutingTable, TempSettings);

			if (ExecutingMenuIconIDs.Num() > 1) {
				ExecutingMenuIconSettings = TempSettings;
				ToggleSelectionMenu(true);
				CreateBindOnSelect();
			}
			else if (ExecutingMenuIconIDs.Num() == 1) {
				ExecutingIcon = ExecutingMenuIconIDs[0];
				RequestPingOnServer();
			}
		}
	}
}

FHitResult UPS_PingComponent::GetHitUsingTraceSettings()
{
	if (TraceMethod == EPS_TraceMethod::CAMERA || TraceMethod == EPS_TraceMethod::MOUSE) {

		FVector WorldLocation;
		FVector WorldDirection;
		UGameplayStatics::GetPlayerController(this, 0)->DeprojectMousePositionToWorld(WorldLocation, WorldDirection);
		FVector StartLocation = TraceMethod == EPS_TraceMethod::CAMERA ? 
			UGameplayStatics::GetPlayerCameraManager(this, 0)->GetCameraLocation() : 
			WorldLocation;
		FVector EndLocation = StartLocation + ((TraceMethod == EPS_TraceMethod::CAMERA ? UGameplayStatics::GetPlayerCameraManager(this, 0)->GetCameraRotation().Vector() : WorldDirection) * TraceLength);

		TArray<AActor*> ActorsToIgnore;
		FHitResult HitResult;
		ActorsToIgnore.Add(OwningPawn);
		UKismetSystemLibrary::SphereTraceSingle(this, StartLocation, EndLocation, TraceRadius, TraceChannel, bTraceComplex, ActorsToIgnore, EDrawDebugTrace::None, HitResult, true);

		return HitResult;
	}

	if (OwningPawn->GetClass()->ImplementsInterface(UPS_Pingable::StaticClass()))
	{
		return IPS_Pingable::Execute_GetCustomTrace(OwningPawn); 
	}
	return FHitResult();
}

void UPS_PingComponent::CreatePing(FName Icon, TArray<FString> Conditions)
{
	bTraceRequired = true;
	HandlePingOnClient(Icon, Conditions);
}

void UPS_PingComponent::CreatePingAtLocation(FName Icon, TArray<FString> Conditions, FVector Location)
{
	ExecutionLocation = Location;
	bTraceRequired = false;
	HandlePingOnClient(Icon, Conditions);
}

void UPS_PingComponent::ClearAllActivePings(bool Force)
{
	for (APS_PingActor* PingActor : ExistingPingActors) {
		if (PingActor->bInUse) {
			PingActor->Deactivate(Force);
		}
	}
}

int UPS_PingComponent::GetUniqueOwnerID()
{
	int TestID = FMath::RandRange(0, 500000);
	
	for (UPS_PingComponent* PingComponent : ExistingPingComponents) {
		if (PingComponent->ID == TestID) {
			return GetUniqueOwnerID();
		}
	}

	return TestID;
}

TArray<FString> UPS_PingComponent::GetDataFromConfig(FString Source, FName& Path)
{
	FString SourceString = Source;

	// Initially split path + domain sections
	// Format is Table / Icon@DomainConditions
	int DomainIndex = SourceString.Find("@", ESearchCase::IgnoreCase, ESearchDir::FromEnd);
	bool HasDomain = DomainIndex >= 0;

	if (HasDomain) {
		FString IDString = SourceString.Mid(0, DomainIndex);
		FString DomainsString = SourceString.Mid(DomainIndex + 1, SourceString.Len());

		TArray<FString> DomainList;
		DomainsString.ParseIntoArray(DomainList, TEXT(","), true);

		TArray<FString> FoundConditions;
		for (FString Domain : DomainList) {
			FoundConditions.AddUnique(Domain);
		}

		Path = FName(IDString);
		return FoundConditions;
	}

	Path = FName(SourceString);
	return TArray<FString>();
}

FString UPS_PingComponent::CreateConfigFromData(FName Table, FName IconID, TArray<FString> Domains)
{
	FString Result;
	Result.Append(Table.ToString());
	Result.Append("/");
	Result.Append(IconID.ToString());

	if (Domains.Num() == 0) {
		return Result;
	}

	Result.Append("@");
	
	for (FString Domain : Domains) {
		Result.Append(Domain);
		Result.Append(",");
	}

	return Result;
}

FPS_TableIconInfo UPS_PingComponent::GetIconAndTableFromPath(FName Path)
{
	FName PathName = Path;
	int SlashIndex = PathName.ToString().Find("/");

	FSoftObjectPath IconTablesPath("DataTable'/Game/PingSystem/IconTables/DTPS_IconTables.DTPS_IconTables'");
	FSoftObjectPath DefaultIconsPath("DataTable'/Game/PingSystem/IconTables/IconList/DTPS_DefaultIcons.DTPS_DefaultIcons'");
	
	if (SlashIndex < 0) {	
		UDataTable* IconTables = (UDataTable*)IconTablesPath.TryLoad();
		FPS_IconTable* OutTable = IconTables->FindRow<FPS_IconTable>(PathName, FString(""));
		if (OutTable) {
			return FPS_TableIconInfo(true, OutTable->Settings, PathName, false, FName("None"), FPS_IconSettings());
		}

		UDataTable* DefaultIcons = (UDataTable*)DefaultIconsPath.TryLoad();
		FPS_IconSettings* OutIcons = DefaultIcons->FindRow<FPS_IconSettings>(PathName, FString(""));
		if (OutIcons) {
			return FPS_TableIconInfo(true, DefaultIcons, FName("Default"), true, PathName, *OutIcons);
		}

	}
	else {
		FName TableName = FName(*(PathName.ToString().Mid(0, SlashIndex)));

		UDataTable* IconTables = (UDataTable*)IconTablesPath.TryLoad();
		FPS_IconTable* OutTable = IconTables->FindRow<FPS_IconTable>(TableName, FString(""));
		
		if (OutTable) {
			UDataTable* IconSettings = OutTable->Settings;
			PathName = FName(*(PathName.ToString().Mid(SlashIndex + 1, PathName.ToString().Len())));

			if (PathName != FName("None")) {
				FPS_IconSettings* OutIcons = IconSettings->FindRow<FPS_IconSettings>(PathName, FString(""));
				if (OutIcons) {
					return FPS_TableIconInfo(true, IconSettings, TableName, true, PathName, *OutIcons);
				}
				else {
					return FPS_TableIconInfo(true, IconSettings, TableName, false, FName("None"), FPS_IconSettings());
				}
			}
			else {
				return FPS_TableIconInfo(true, IconSettings, TableName, false, FName("None"), FPS_IconSettings());;
			}
		}
	}

	return FPS_TableIconInfo(false, (UDataTable*)DefaultIconsPath.TryLoad(), FName("Default"), false, FName("None"), FPS_IconSettings());
}

bool UPS_PingComponent::IsValidExecutionDomain(TArray<FString> Valid, TArray<FString> Conditions)
{
	TArray<FString> SourceDomains = Valid;

	if (Conditions.Num() > 0) {
		for (FString Condition : Conditions) {
			TArray<FString> AndArray;
			Condition.ParseIntoArray(AndArray, TEXT("&"), true);

			if (AndArray.Num() > 0) {
				if (EvaluateDomainConditions(SourceDomains, AndArray)) {
					return true;
				}
			}
			else {
				return true;
			}
		
		}
		return false;
	}

	return true;
}

bool UPS_PingComponent::EvaluateDomainConditions(TArray<FString> Source, TArray<FString> Condition)
{
	TArray<FString> SourceDomains = Source;
	
	for (FString Cond : Condition) {
		// Search for whether it exists in ValidDomains list
		FString DomainToFind;
		bool ConditionRequired = GetDomainConditionRequired(Cond, DomainToFind);
		
		int TempIndex = SourceDomains.Find(DomainToFind);

		bool IsValidDomain = (TempIndex >= 0 && ConditionRequired) || (TempIndex < 0 && !ConditionRequired);

		if (!IsValidDomain) {
			return false;
		}
	}

	return true;
}

TArray<FName> UPS_PingComponent::GetMenuIconsFromTable(FName Table, TArray<FPS_IconSettings>& Settings)
{
	FSoftObjectPath IconTablesPath("DataTable'/Game/PingSystem/IconTables/DTPS_IconTables.DTPS_IconTables'");

	UDataTable* IconTables = (UDataTable*)IconTablesPath.TryLoad();
	FPS_IconTable* OutTable = IconTables->FindRow<FPS_IconTable>(Table, FString(""));
	TArray<FName> FoundIDs;

	TArray<FName> IconTableRows;
	
	UDataTableFunctionLibrary::GetDataTableRowNames(OutTable->Settings, IconTableRows);

	TArray<FPS_IconSettings> FoundIcons;
	

	for (FName IconRow : IconTableRows) {
		FPS_IconSettings Setting = *(OutTable->Settings->FindRow<FPS_IconSettings>(IconRow, FString("")));

		if (Setting.Advanced.ShowInSelectionMenu) {
			FoundIcons.Add(Setting);
			FoundIDs.Add(IconRow);
		}
	}

	Settings = FoundIcons;
	return FoundIDs;
}

void UPS_PingComponent::HandleActiveCulling(APS_PingActor* NewPingActor)
{
	ActivePingsOnClient.Add(NewPingActor);

	TArray<APS_PingActor*> TempActivePingArray = ActivePingsOnClient;
	
	APS_PingActor* CurrentPing;
	int TotalOwnedVisible = 0;
	int TotalNonOwnedVisible = 0;

	TMap<int, int> VisiblePingsByClient;

	for (int i = TempActivePingArray.Num() - 1; i >= 0; i--) {
		CurrentPing = TempActivePingArray[i];

		if (CurrentPing->OwnerID == ID) {
			TotalOwnedVisible++;

			if (MaxVisiblePingsOwner >= 0 && TotalOwnedVisible > MaxVisiblePingsOwner) {
				CurrentPing->Deactivate();
				ActivePingsOnClient.Remove(CurrentPing);
			}
		}
		else {
			TotalNonOwnedVisible++;

			VisiblePingsByClient.Add(CurrentPing->OwnerID, (VisiblePingsByClient.Contains(CurrentPing->OwnerID) ? VisiblePingsByClient[CurrentPing->OwnerID] : 0) + 1);

			// DO NOT GENERATE NEW PING LOCALLY IF:
			// 1. Your Visible TOTAL NON-OWNED PINGS Exceed the maximum you can handle
			// 2. The Invoking client's ping exceed their total ping
			if ( ((MaxVisiblePingsNonOwnedTotal >= 0) && (TotalNonOwnedVisible > MaxVisiblePingsNonOwnedTotal)) || 
				((MaxVisiblePingsNonOwnedPerClient >= 0) && (VisiblePingsByClient[CurrentPing->OwnerID] > MaxVisiblePingsNonOwnedPerClient))) {
				CurrentPing->Deactivate();
				ActivePingsOnClient.Remove(CurrentPing);
			}
		}
	}
}

bool UPS_PingComponent::CheckRateLimiting()
{
	TArray<float> NewTimeouts = PingTimeouts;

	for (int i = 0; i < NewTimeouts.Num(); i++) {
		if (GetWorld()->GetTimeSeconds() >= NewTimeouts[i]) {
			NewTimeouts.RemoveAt(i);
			i--;
		}
	}

	PingTimeouts = NewTimeouts;

	if (((PingTimeouts.Num() >= MaxActivePingRequests) && ((GetWorld()->GetTimeSeconds() - Timeout) >= (TimeoutOnMaxRequestsReached))) || 
		((PingTimeouts.Num() < MaxActivePingRequests) && ((GetWorld()->GetTimeSeconds() - Timeout) >= TimeoutBetweenPingRequests	))) {
		Timeout = GetWorld()->GetTimeSeconds();
		return true;
	}
	
	return false;
}

bool UPS_PingComponent::IsValidVisibleDistance(FVector Source, FVector Target, float MinDistance, float MaxDistance)
{
	return ((Source - Target).Size() >= MinDistance || MinDistance == 0.0f) && 
		((Source - Target).Size() <= MaxDistance || MaxDistance == 0.0f);
}

void UPS_PingComponent::OnOwnerDestroyed(AActor* DestroyedActor)
{
	ClearPingPool();
}

void UPS_PingComponent::CreateBindOnSelect()
{
	SelectionMenu->Selected.AddDynamic(this, &UPS_PingComponent::OnSelected);
}

void UPS_PingComponent::OnSelected(FPS_IconSettings Selected, FName IconID)
{
	ExecutingIcon = IconID;
	RequestPingOnServer();
	SelectionMenu->Selected.Clear();
}

int UPS_PingComponent::ClampIntegerToRange(int Value, int Min, int Max, bool Wrap)
{
	return Wrap ? FMath::Clamp(Value, Min, Max) : (Value < Min ? Max : (Value > Max ? Min : Value));
}

bool UPS_PingComponent::GetDomainConditionRequired(FString String, FString& ParsedString)
{
	int StringIndex = String.Find("!");

	bool Required = StringIndex < 0;
	ParsedString = Required ? String : String.Mid(StringIndex + 1, String.Len());

	return Required;
}

#undef LOCTEXT_NAMESPACE 
