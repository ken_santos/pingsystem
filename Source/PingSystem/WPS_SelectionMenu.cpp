// Fill out your copyright notice in the Description page of Project Settings.


#include "WPS_SelectionMenu.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "PS_PingComponent.h"

void UWPS_SelectionMenu::ConfirmSelection()
{
	RemoveFromParent();
}

void UWPS_SelectionMenu::CancelSelection()
{
	SelectedIndex = -1;
	RemoveFromParent();
}
