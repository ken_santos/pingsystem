// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "PS_Structs.h"
#include "PS_PingComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FDefaultPingComponentDelegate);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FRequestOnServerDelegate, FVector, Location, FString, Config, AActor*, AttachedActor);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FExecuteOwningClientDelegate, FVector, Location, FString, Config, int, Owner, AActor*, AttachedActor);



UCLASS(Blueprintable, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class PINGSYSTEM_API UPS_PingComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UPS_PingComponent();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General Settings")
	TSubclassOf<class UWPS_SelectionMenu> SelectionMenuToUse;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General Settings")
	TSubclassOf<class APS_PingActor> PingActorToUse;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General Settings")
	int PingPoolSize;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General Settings")
	TArray<FLinearColor> TintColorList;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General Settings")
	bool bAutoExpandPool;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated, Category = "General Settings")
	int TintColorIndex;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "General Settings")
	EPS_MenuSelectionType SelectionMode;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Visibility Settings")
	TArray<FString> ValidDomains;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Visibility Settings")
	float MinVisibleDistance;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Visibility Settings")
	float MaxVisibleDistance;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Spam Control")
	int MaxVisiblePingsOwner;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Spam Control")
	int MaxVisiblePingsNonOwnedTotal;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Spam Control")
	int MaxVisiblePingsNonOwnedPerClient;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Spam Control")
	float TimeoutBetweenPingRequests;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Spam Control")
	int MaxActivePingRequests;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Spam Control")
	float TimeoutOnMaxRequestsReached;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Trace Settings")
	EPS_TraceMethod TraceMethod;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Trace Settings")
	float TraceLength;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Trace Settings")
	float TraceRadius;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Trace Settings")
	TEnumAsByte<ETraceTypeQuery> TraceChannel;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Trace Settings")
	bool bTraceComplex;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Advanced Settings")
	bool bExecuteLocallyOnly;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Menu Settings")
	bool bEnableMenu;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Menu Settings")
	FLinearColor BaseColor;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Menu Settings")
	FLinearColor HoverColor;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Menu Settings")
	FLinearColor HighlightColor;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Menu Settings")
	float HighlightThickness;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Menu Settings")
	FLinearColor BorderColor;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Menu Settings")
	float BorderThickness;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Others")
	class APawn* OwningPawn;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Others")
	TArray<class UPS_PingComponent*> ExistingPingComponents;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Others")
	TArray<class APS_PingActor*> ExistingPingActors;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Others")
	TArray<class APS_PingActor*> ActivePingsOnClient;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Others")
	TArray<float> PingTimeouts;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Others")
	float Timeout;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated, Category = "Others")
	FLinearColor TintColor;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Replicated, Category = "Others")
	int ID;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Others")
	FName ExecutingTable;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Others")
	FName ExecutingIcon;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Others")
	TArray<FString> ExecutingConditions;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Others")
	FVector ExecutionLocation;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Others")
	class AActor* ExecutionAttachedActor;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Others")
	FName OverrideIcon;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Others")
	TArray<FString> OverrideConditions;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Others")
	bool bTraceRequired;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Others")
	TArray<FName> ExecutingMenuIconIDs;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Others")
	TArray<FPS_IconSettings> ExecutingMenuIconSettings;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Others")
	FPS_IconSettings ExecutingPingSettings;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Others")
	class UWPS_SelectionMenu* SelectionMenu;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Others")
	bool bDebug;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = "System")
	void Setup();

	UFUNCTION(BlueprintCallable, Category = "System")
	void UpdateAllPingComponentReferences();

	UFUNCTION(BlueprintCallable, Category = "System")
	class APS_PingActor* CreatePingActorForPool();

	UFUNCTION(BlueprintCallable, Category = "System")
	void CreateInitialPingPool();

	UFUNCTION(BlueprintCallable, Category = "System")
	void ClearPingPool();

	UFUNCTION(BlueprintCallable, Category = "System")
	class APS_PingActor* GetUnusedPingActorFromPool();

	UFUNCTION(BlueprintCallable, Category = "Selection Menu")
	class UWPS_SelectionMenu* ToggleSelectionMenu(bool Visible);

	UFUNCTION(BlueprintCallable, Category = "Selection Menu")
	void ConfirmSelectionInMenu();

	UFUNCTION(BlueprintCallable, Category = "Selection Menu")
	void CancelSelectionInMenu();

	UFUNCTION(BlueprintCallable, Category = "Core")
	void ExecutePingOnClient(FVector Location, FString Config, int OwnerID, class AActor* AttachedActor);

	UFUNCTION(BlueprintCallable, Category = "Core")
	void HandlePingOnServer(FVector Location, FString Config, class AActor* AttachedActor);

	UFUNCTION(BlueprintCallable, Category = "Core")
	void HandlePingOnClient(FName Icon, TArray<FString> Conditions);

	UFUNCTION(BlueprintCallable, Category = "Core")
	FHitResult GetHitUsingTraceSettings();

	UFUNCTION(BlueprintCallable, Category = "Core")
	void CreatePing(FName Icon, TArray<FString> Conditions);

	UFUNCTION(BlueprintCallable, Category = "Core")
	void CreatePingAtLocation(FName Icon, TArray<FString> Conditions, FVector Location);

	UFUNCTION(BlueprintCallable, Category = "Core")
	void ClearAllActivePings(bool Force);

	UFUNCTION(BlueprintCallable, Category = "Utility")
	int GetUniqueOwnerID();

	UFUNCTION(BlueprintCallable, Category = "Utility")
	TArray<FString> GetDataFromConfig(FString Source, FName& Path);

	UFUNCTION(BlueprintCallable, Category = "Utility")
	FString CreateConfigFromData(FName Table, FName IconID, TArray<FString> Domains);

	UFUNCTION(BlueprintCallable, Category = "Utility")
	FPS_TableIconInfo GetIconAndTableFromPath(FName Path);

	UFUNCTION(BlueprintCallable, Category = "Utility")
	bool IsValidExecutionDomain(TArray<FString> Valid, TArray<FString> Conditions);
	
	UFUNCTION(BlueprintCallable, Category = "Utility")
	bool EvaluateDomainConditions(TArray<FString> Source, TArray<FString> Condition);

	UFUNCTION(BlueprintCallable, Category = "Utility")
	TArray<FName> GetMenuIconsFromTable(FName Table, TArray<FPS_IconSettings>& Settings);

	UFUNCTION(BlueprintCallable, Category = "Spam & Culling")
	void HandleActiveCulling(APS_PingActor* NewPingActor);

	UFUNCTION(BlueprintCallable, Category = "Spam & Culling")
	bool CheckRateLimiting();

	UFUNCTION(BlueprintCallable, Category = "Spam & Culling")
	bool IsValidVisibleDistance(FVector Source, FVector Target, float MinDistance, float MaxDistance);

	UFUNCTION(BlueprintCallable)
	void OnOwnerDestroyed(AActor* DestroyedActor);
	
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent)
	void RequestPingOnServer();

	UFUNCTION(BlueprintCallable)
	void CreateBindOnSelect();

	UFUNCTION(BlueprintCallable)
	void OnSelected(FPS_IconSettings Selected, FName IconID);

// Event Dispatchers
public:
	UPROPERTY(BlueprintCallable, BlueprintAssignable, Category = "Event Dispatchers")
	FRequestOnServerDelegate RequestOnServer;

	UPROPERTY(BlueprintCallable, BlueprintAssignable, Category = "Event Dispatchers")
	FExecuteOwningClientDelegate ExecuteOwningClient;

	UPROPERTY(BlueprintCallable, BlueprintCallable, BlueprintAssignable, Category = "Event Dispatchers")
	FDefaultPingComponentDelegate MenuActivated;

	UPROPERTY(BlueprintCallable, BlueprintAssignable, Category = "Event Dispatchers")
	FDefaultPingComponentDelegate MenuDeactivated;

	UPROPERTY(BlueprintCallable, BlueprintAssignable, Category = "Event Dispatchers")
	FDefaultPingComponentDelegate PingReceivedOnServer;

private:
	int ClampIntegerToRange(int Value, int Min, int Max, bool Wrap);
	bool GetDomainConditionRequired(FString String, FString& ParsedString);
};
